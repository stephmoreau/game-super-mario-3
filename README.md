## Super Mario 3 Mini Game Style Game ##
*Only Html and CSS were used to create this game.*
Inspired by student project. I attempt to make my own version of a games they have submitted using only HTML and CSS.
 
This is a mini game that is feature in the NES game Super Mario Bros 3. The point of the game is to line up the images in order to produce the correct image combination.

---

## Extra files used
List of files besides out index.html and style.css
1. Used graphics online to create the images in the slider (http://www.mariouniverse.com/wp-content/img/sprites/gba/smb3/slots.gif)

2. Font Super Mario Script 3 (https://www.ffonts.net/Super-Mario-Script-3.font)

---

## JavaScript
If I had include JavScript, what would be different?

1. I would be able to have the animation slow down and stop at the next closest image.
2. I would be able to determine if all 3 images maatched up properly

---

## Future Tasks ##
Notes on possible options/functionality that could be added in HTML or CSS

1. Create the imagees in the slider using only css?
